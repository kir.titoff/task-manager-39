package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.ktitov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.ktitov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Nullable
    private Project project;

    @Nullable
    private String userLogin(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    private void userLogout(@Nullable final String token) {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpoint.logout(request);
    }

    @Nullable
    private Project projectCreate(@NotNull final String name, @NotNull final String description) {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName(name);
        request.setDescription(description);
        return projectEndpoint.createProject(request).getProject();
    }

    private void projectRemove(@NotNull final String id) {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(id);
        projectEndpoint.removeProjectById(request);
    }

    @Before
    public void setUp() {
        userToken = userLogin("admin", "admin");
        project = projectCreate("name", "description");
    }

    @After
    public void tearDown() {
        projectRemove(project.getId());
        userLogout(userToken);
    }

    @Test
    public void dataBackupSaveLoad() {
        @NotNull final DataBackupSaveRequest requestSave = new DataBackupSaveRequest(userToken);
        domainEndpoint.saveDataBackup(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataBackupLoadRequest requestLoad = new DataBackupLoadRequest(userToken);
        domainEndpoint.loadDataBackup(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataBase64SaveLoad() {
        @NotNull final DataBase64SaveRequest requestSave = new DataBase64SaveRequest(userToken);
        domainEndpoint.saveDataBase64(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataBase64LoadRequest requestLoad = new DataBase64LoadRequest(userToken);
        domainEndpoint.loadDataBase64(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataBinarySaveLoad() {
        @NotNull final DataBinarySaveRequest requestSave = new DataBinarySaveRequest(userToken);
        domainEndpoint.saveDataBinary(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataBinaryLoadRequest requestLoad = new DataBinaryLoadRequest(userToken);
        domainEndpoint.loadDataBinary(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataJsonFasterXmlSaveLoad() {
        @NotNull final DataJsonFasterXmlSaveRequest requestSave = new DataJsonFasterXmlSaveRequest(userToken);
        domainEndpoint.saveDataJsonFasterXml(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataJsonFasterXmlLoadRequest requestLoad = new DataJsonFasterXmlLoadRequest(userToken);
        domainEndpoint.loadDataJsonFasterXml(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataJsonJaxBSaveLoad() {
        @NotNull final DataJsonJaxBSaveRequest requestSave = new DataJsonJaxBSaveRequest(userToken);
        domainEndpoint.saveDataJsonJaxB(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataJsonJaxBLoadRequest requestLoad = new DataJsonJaxBLoadRequest(userToken);
        domainEndpoint.loadDataJsonJaxB(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataXmlFasterXmlSaveLoad() {
        @NotNull final DataXmlFasterXmlSaveRequest requestSave = new DataXmlFasterXmlSaveRequest(userToken);
        domainEndpoint.saveDataXmlFasterXml(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataXmlFasterXmlLoadRequest requestLoad = new DataXmlFasterXmlLoadRequest(userToken);
        domainEndpoint.loadDataXmlFasterXml(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataXmlJaxBSaveLoad() {
        @NotNull final DataXmlJaxBSaveRequest requestSave = new DataXmlJaxBSaveRequest(userToken);
        domainEndpoint.saveDataXmlJaxB(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataXmlJaxBLoadRequest requestLoad = new DataXmlJaxBLoadRequest(userToken);
        domainEndpoint.loadDataXmlJaxB(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

    @Test
    public void dataYamlFasterXmlSaveLoad() {
        @NotNull final DataYamlFasterXmlSaveRequest requestSave = new DataYamlFasterXmlSaveRequest(userToken);
        domainEndpoint.saveDataYamlFasterXml(requestSave);
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        @NotNull final DataYamlFasterXmlLoadRequest requestLoad = new DataYamlFasterXmlLoadRequest(userToken);
        domainEndpoint.loadDataYamlFasterXml(requestLoad);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @Nullable final Project foundProject = projectEndpoint.getProjectById(requestGet).getProject();
        Assert.assertEquals("name", foundProject.getName());
        Assert.assertEquals("description", foundProject.getDescription());
    }

}
