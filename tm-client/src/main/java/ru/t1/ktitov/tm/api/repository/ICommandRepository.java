package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable final AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable final String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String name);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
