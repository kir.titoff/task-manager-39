package ru.t1.ktitov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.ktitov.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractResponse {
}
