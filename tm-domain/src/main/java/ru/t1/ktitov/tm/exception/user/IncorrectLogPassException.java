package ru.t1.ktitov.tm.exception.user;

public final class IncorrectLogPassException extends AbstractUserException {

    public IncorrectLogPassException() {
        super("Error! Incorrect login or password entered. Please try again.");
    }

}
