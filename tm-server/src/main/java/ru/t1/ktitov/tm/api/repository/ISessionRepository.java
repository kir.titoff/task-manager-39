package ru.t1.ktitov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, role, user_id) " +
            "VALUES (#{id}, #{date}, #{role}, #{userId})")
    void addSession(@NotNull final Session session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clearSessionsByUser(@Nullable final String userId);

    @Delete("TRUNCATE TABLE tm_session")
    void clearSessions();

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAllSessionsByUser(@Nullable final String userId);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAllSessionsByUserWithOrder(@Nullable final String userId, @Nullable final String orderBy);

    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAllSessions();

    @Select("SELECT * FROM tm_session ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAllSessionsWithOrder(@Nullable final String orderBy);

    @Select("SELECT * FROM tm_session WHERE ID = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session findSessionByIdByUser(@Param("userId") @Nullable final String userId, @Param("id") @Nullable final String id);

    @Select("SELECT * FROM tm_session WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session findSessionById(@Nullable final String id);

    @Select("SELECT COUNT(1) FROM tm_session WHERE user_id = #{userId}")
    int getSessionsSizeByUser(@Nullable final String userId);

    @Select("SELECT COUNT(1) FROM tm_session")
    int getSessionsSize();

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeSession(@Nullable final Session session);

    @Delete("DELETE FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    void removeSessionByIdByUser(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeSessionById(@NotNull final String id);

}
