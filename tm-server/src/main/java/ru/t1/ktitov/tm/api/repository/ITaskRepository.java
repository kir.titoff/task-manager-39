package ru.t1.ktitov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void addTask(@NotNull final Task task);

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, status = #{status}, " +
            "project_id = #{projectId} WHERE id = #{id}")
    void updateTask(@Nullable final Task task);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllTasksByProjectId(
            @Param("userId") @Nullable final String userId,
            @Param("projectId") @Nullable final String projectId
    );

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearTasksByUser(@Nullable final String userId);

    @Delete("TRUNCATE TABLE tm_task")
    void clearTasks();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllTasksByUser(@Nullable final String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllTasksByUserWithOrder(@Nullable final String userId, @Nullable final String orderBy);

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllTasks();

    @Select("SELECT * FROM tm_task ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllTasksWithOrder(@Nullable final String orderBy);

    @Select("SELECT * FROM tm_task WHERE ID = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findTaskByIdByUser(@Param("userId") @Nullable final String userId, @Param("id") @Nullable final String id);

    @Select("SELECT * FROM tm_task WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findTaskById(@Nullable final String id);

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getTasksSizeByUser(@Nullable final String userId);

    @Select("SELECT COUNT(1) FROM tm_task")
    int getTasksSize();

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeTask(@Nullable final Task task);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeTaskByIdByUser(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeTaskById(@NotNull final String id);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeTasksByUser(@NotNull final String userId);

}
