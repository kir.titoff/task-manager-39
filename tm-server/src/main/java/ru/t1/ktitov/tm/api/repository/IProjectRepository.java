package ru.t1.ktitov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})")
    void addProject(@NotNull final Project project);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, " +
            "STATUS = #{status} WHERE ID = #{id}")
    void updateProject(@NotNull final Project project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearProjectsByUser(@Nullable final String userId);

    @Delete("TRUNCATE TABLE tm_project")
    void clearProjects();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllProjectsByUser(@Nullable final String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllProjectsByUserWithOrder(@Nullable final String userId, @Nullable final String orderBy);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllProjects();

    @Select("SELECT * FROM tm_project ORDER BY #{orderBy}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllProjectsWithOrder(@Nullable final String orderBy);

    @Select("SELECT * FROM tm_project WHERE ID = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Project findProjectByIdByUser(@Param("userId") @Nullable final String userId, @Param("id") @Nullable final String id);

    @Select("SELECT * FROM tm_project WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Project findProjectById(@Nullable final String id);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getProjectsSizeByUser(@Nullable final String userId);

    @Select("SELECT COUNT(1) FROM tm_project")
    int getProjectsSize();

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeProject(@Nullable final Project project);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeProjectByIdByUser(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeProjectById(@NotNull final String id);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeProjectsByUser(@NotNull final String userId);

}
