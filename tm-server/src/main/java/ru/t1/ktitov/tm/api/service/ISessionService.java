package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    Collection<Session> set(@NotNull Collection<Session> sessions);

    @Nullable
    Session add(@Nullable Session session);

    @Nullable
    Session add(@Nullable String userId, @Nullable Session session);

    @NotNull
    Collection<Session> add(@NotNull Collection<Session> sessions);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    Session remove(@Nullable Session session);

    @Nullable
    Session remove(@Nullable String userId, @Nullable Session session);

    @Nullable
    Session removeById(@Nullable String id);

    @Nullable
    Session removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAll(@Nullable String userId);

    @Nullable
    List<Session> findAll(@Nullable Sort sort);

    @Nullable
    List<Session> findAll(@Nullable String userId, @Nullable Sort sort);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    Session findOneById(@Nullable String id);

    Session findOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

}
