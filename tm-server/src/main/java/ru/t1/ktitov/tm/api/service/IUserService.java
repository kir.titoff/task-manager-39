package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    Collection<User> set(@NotNull Collection<User> users);

    @Nullable
    User add(@Nullable User user);

    @NotNull
    Collection<User> add(@NotNull Collection<User> users);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User updateUser(
            @Nullable String id, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

    void clear();

    @Nullable
    User remove(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    List<User> findAll();

    @NotNull
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean existsById(@Nullable String id);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    int getSize();

}
