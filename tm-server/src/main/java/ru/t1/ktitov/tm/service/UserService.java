package ru.t1.ktitov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.exception.user.UserEmailExistsException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<User> set(@Nullable Collection<User> users) {
        if (users == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.clearUsers();
            users.forEach(repository::addUser);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<User> add(@Nullable Collection<User> users) {
        if (users == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            users.forEach(repository::addUser);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return users;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new UserEmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.addUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.clearUsers();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User remove(@Nullable final User user) {
        if (user == null) throw new EntityNotFoundException();
        @Nullable final User foundUser;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            foundUser = userRepository.findUserById(user.getId());
            userRepository.removeUser(user);
            projectRepository.removeProjectsByUser(user.getId());
            taskRepository.removeTasksByUser(user.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return foundUser;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            user = repository.findUserById(id);
            repository.removeUserById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findAllUsers();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final User user = repository.findUserById(id);
            if (user == null) throw new EntityNotFoundException();
            return user;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findUserByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findUserByEmail(email);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findUserById(id) != null;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findUserByLogin(login) != null;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findUserByEmail(email) != null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.getUsersSize();
        } finally {
            sqlSession.close();
        }
    }

}
