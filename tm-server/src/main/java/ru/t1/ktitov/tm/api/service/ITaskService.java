package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> tasks);

    @Nullable
    Task add(@Nullable Task task);

    @Nullable
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> tasks);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    Task remove(@Nullable Task task);

    @Nullable
    Task remove(@Nullable String userId, @Nullable Task task);

    @Nullable
    Task removeById(@Nullable String id);

    @Nullable
    Task removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable Sort sort);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    Task findOneById(@Nullable String id);

    Task findOneById(@Nullable String userId, @Nullable String id);

    int getSize(@Nullable String userId);

    int getSize();

}
