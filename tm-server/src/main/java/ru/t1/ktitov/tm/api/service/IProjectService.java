package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> projects);

    @Nullable
    Project add(@Nullable Project project);

    @Nullable
    Project add(@Nullable String userId, @Nullable Project project);

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    Project remove(@Nullable Project project);

    @Nullable
    Project remove(@Nullable String userId, @Nullable Project project);

    @Nullable
    Project removeById(@Nullable String id);

    @Nullable
    Project removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable Sort sort);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    Project findOneById(@Nullable String id);

    Project findOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSize(@Nullable String userId);

}
